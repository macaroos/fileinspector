//
//  DirectoryChooserViewController.m
//  FileInspector
//
//  Created by Steve Hartwell on 8/30/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

#import "DirectoryChooserViewController.h"

@interface NSFileManager (SHHExtensions)
+ (BOOL)isDirectory: (NSString*)path;
@end

@interface SHHDirectoryChooserViewController ()
{
    BOOL _handlingCommand;
    BOOL _processingAutocomplete;
}
@property (weak) IBOutlet NSTextField* pathTextField;

@property (nonatomic, getter=isPathValid) BOOL pathValid;

@end

@implementation SHHDirectoryChooserViewController

- (IBAction)chooseDirectory: (id)sender
{
    NSOpenPanel* openPanel = [NSOpenPanel openPanel];
    openPanel.canChooseDirectories = YES;
    openPanel.canChooseFiles = NO;
    openPanel.allowsMultipleSelection = NO;
    // openPanel.resolvesAliases = NO;

    NSString* currentPath = self.pathTextField.stringValue;
    // TODO: try to make a valid file URL, by dropping lastPathComponent, etc
    if (currentPath.length) {
        openPanel.directoryURL = [NSURL fileURLWithPath:currentPath];
    }
    
    [openPanel
        beginSheetModalForWindow:self.view.window
        completionHandler:
            ^(NSInteger returnCode) {
                if (returnCode != NSFileHandlingPanelOKButton) {
                    return;
                }
                self.pathTextField.stringValue = [openPanel.URLs[0] path];
            }];
}

- (IBAction)inspect: (id)sender
{
    NSWindow* window = [sender window];
    if (window.sheetParent) {
        [window.sheetParent endSheet:window returnCode:NSModalResponseContinue];
    }
    else {
        [window close];
    }
}

- (IBAction)cancel: (id)sender
{
    NSWindow* window = [sender window];
    if (window.sheetParent) {
        [window.sheetParent endSheet:window returnCode:NSModalResponseAbort];
    }
    else {
        [window close];
    }
}

- (BOOL)isPathValid
{
    return [NSFileManager isDirectory:self.pathTextField.stringValue];
}

- (NSURL*)directoryURL
{
    return self.isPathValid
                ? [NSURL fileURLWithPath:self.pathTextField.stringValue]
                : nil;
}

// NSTextViewDelegate

#pragma mark -
#pragma mark @protocol NSControlTextEditingDelegate

- (NSArray*)control: (NSControl*)control
           textView: (NSTextView*)textView
        completions: (NSArray*)words
forPartialWordRange: (NSRange)charRange
indexOfSelectedItem: (NSInteger*)index
{
    NSString* currentPath = self.pathTextField.stringValue;
    NSString* completedPath = nil;
    NSArray* pathMatches = nil;
    NSUInteger count =
        [currentPath completePathIntoString:&completedPath
                       caseSensitive:NO
                    matchesIntoArray:&pathMatches
                         filterTypes:nil];

    if (count == 0) {
        return nil;
    }

    NSMutableArray* completions = [NSMutableArray arrayWithCapacity:count];
    for (NSString* aPath in pathMatches) {
        NSUInteger options =
            (NSAnchoredSearch | NSCaseInsensitiveSearch);
        NSRange prefixRange =
            [aPath rangeOfString:currentPath options:options];
        if (prefixRange.location != NSNotFound) {
            if ([NSFileManager isDirectory:aPath]) {
                [completions addObject:
                    [aPath substringFromIndex:charRange.location]];
            }
        }
    }

    return completions;
}

-     (BOOL)control: (NSControl *)control
           textView: (NSTextView *)textView
doCommandBySelector: (SEL)commandSelector
//
// This delegate method intercepts editing commands;
// specifically, tab handling,
// and more generally, to set a flag to suspend autocomplete
// while a command is being processed.
{
    if (control.window == self.view.window) {
        if (commandSelector == @selector(insertTab:)) {
            // ...
            return YES;
        }
    }
    
    // Here for all other command handling.
    // We want to set a flag so that -controlTextDidChange:
    // doesn't compete with other commands which change the text
    
    BOOL didPerformRequestedSelectorOnTextView = NO;
    if ([textView respondsToSelector:commandSelector])
    {
        _handlingCommand = YES;
        //
        NSMethodSignature *textViewSelectorMethodSignature =
            [textView methodSignatureForSelector:commandSelector];
        NSInvocation *textViewInvocationForSelector =
            [NSInvocation
                invocationWithMethodSignature:textViewSelectorMethodSignature];
        [textViewInvocationForSelector setTarget:textView];
        [textViewInvocationForSelector setSelector:commandSelector];
        //
        [textViewInvocationForSelector invoke];
        //
        _handlingCommand = NO;
        
        didPerformRequestedSelectorOnTextView = YES;
    }
    
    return didPerformRequestedSelectorOnTextView;
}

#pragma mark -
#pragma NSControl informal protocol

- (void)controlTextDidChange: (NSNotification*)note
{
    self.pathValid = self.isPathValid; // todo: find a less goofy way to emit

    // avoid calling -complete: when other processing is going on
    if (_handlingCommand || _processingAutocomplete) {
        return;
    }

    _processingAutocomplete = YES;

    NSTextView* textView = [[note userInfo] objectForKey:@"NSFieldEditor"];
    [textView complete:self];

    _processingAutocomplete = NO;
}

@end


@implementation NSFileManager (SHHExtensions)
+ (BOOL)isDirectory: (NSString*)path
{
    NSFileManager* fileManager = [[self class] defaultManager];
    BOOL isDir;
    
    return [fileManager fileExistsAtPath:path isDirectory:&isDir] && isDir;
}
@end
