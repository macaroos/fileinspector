//
//  FileModel.h
//  FileInspector
//
//  Created by Steve Hartwell on 8/29/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

// This class derives from the Xcode-generated FileEntity
// which is defined in FileInspectionModel.xcdatamodeld


#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "entities/SHHFileEntity.h"


@interface SHHFileModel: SHHFileEntity

+ (NSEntityDescription*)entityInContext: (NSManagedObjectContext*)context;

+ (NSDictionary*)attributeMap;

+ (instancetype)modelInManagedObjectContext: (NSManagedObjectContext*)context
                                    withURL: (NSURL*)fileURL;

@property (readonly, strong) NSURL* url;

@end
