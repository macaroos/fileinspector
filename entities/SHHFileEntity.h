//
//  SHHFileEntity.h
//  FileInspector
//
//  Created by Steve Hartwell on 9/8/15.
//  Copyright (c) Steve Hartwell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SHHFileEntity : NSManagedObject

@property (nonatomic, retain) id fileicon;
@property (nonatomic, retain) NSString * filekind;
@property (nonatomic, retain) NSString * filename;
@property (nonatomic, retain) NSString * filepath;
@property (nonatomic, retain) NSNumber * filesize;
@property (nonatomic, retain) NSNumber * isFolder;
@property (nonatomic, retain) NSNumber * isPackage;
@property (nonatomic, retain) NSDate * filecreated;
@property (nonatomic, retain) NSDate * filecontentacc;
@property (nonatomic, retain) NSDate * filecontentmod;
@property (nonatomic, retain) NSDate * fileattrmod;

@end
