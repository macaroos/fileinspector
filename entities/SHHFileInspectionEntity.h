//
//  SHHFileInspectionEntity.h
//  FileInspector
//
//  Created by Steve Hartwell on 8/31/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SHHFileInspectionEntity : NSManagedObject

@property (nonatomic, retain) NSString * url;

@end
