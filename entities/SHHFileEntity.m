//
//  SHHFileEntity.m
//  FileInspector
//
//  Created by Steve Hartwell on 9/8/15.
//  Copyright (c) Steve Hartwell. All rights reserved.
//

#import "SHHFileEntity.h"


@implementation SHHFileEntity

@dynamic fileicon;
@dynamic filekind;
@dynamic filename;
@dynamic filepath;
@dynamic filesize;
@dynamic isFolder;
@dynamic isPackage;
@dynamic filecreated;
@dynamic filecontentacc;
@dynamic filecontentmod;
@dynamic fileattrmod;

@end
