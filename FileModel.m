//
//  FileModel.m
//  FileInspector
//
//  Created by Steve Hartwell on 8/29/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

// This class is a lightweight facade for an NSManagedObject,
// which is the basic object type of Core Data.
//

// All of the data members of this class are transient,
// except the NSURL that it is constructed with.
// That URL is loaded up with all kinds of metadata gathered while
// the directory is being enumerated.
// So this class just extracts them as needed to provide the values.

#import "FileModel.h"

@interface SHHFileModel ()

@property (strong) NSURL* url;

@end

@implementation SHHFileModel

@synthesize url; // needed because NSManagedObject disables auto-synthesize

+ (instancetype)modelInManagedObjectContext: (NSManagedObjectContext*)context
                                    withURL: (NSURL*)fileURL;
// convenience method
// Construct a file model with its all-important URL loaded with attributes.
// The instance will be retained by the managed object context.
{
    NSEntityDescription* entity = [self entityInContext:context];

    SHHFileModel* model =
        [[self alloc]
            initWithEntity:entity insertIntoManagedObjectContext:context];
    if (model) {
        model.url = fileURL;
    }
    return model;
}

+ (NSEntityDescription*)entityInContext: (NSManagedObjectContext*)context
{
    return [NSEntityDescription entityForName:@"FileModel"
                       inManagedObjectContext:context];
}


+ (NSDictionary*)attributeMap
{
    static NSDictionary* _attributeMap = nil;
    if (_attributeMap == nil) {
        _attributeMap = @{
            @"filename":        NSURLLocalizedNameKey,
            @"fileicon":        NSURLEffectiveIconKey,
            @"filekind":        NSURLLocalizedTypeDescriptionKey,
            @"filesize":        NSURLTotalFileSizeKey, // not NSURLFileSizeKey
            @"filecreated":     NSURLCreationDateKey,
            @"filecontentacc":  NSURLContentAccessDateKey,
            @"filecontentmod":  NSURLContentModificationDateKey,
            @"fileattrmod":     NSURLAttributeModificationDateKey,
            @"isFolder":        NSURLIsDirectoryKey,
            @"isPackage":       NSURLIsPackageKey,
        };
    }
    return _attributeMap;
}

- (NSString*)primitiveFilepath
{
    return [self.url path];
}

- (id)valueForKey: (NSString*)key
{
    NSString* urlKey = self.class.attributeMap[key];
    if (urlKey == nil) {
        return [super valueForKey:key];
    }

    id value = nil;
    [self.url getResourceValue:&value forKey:urlKey error:NULL];

    return value;
}

@end
