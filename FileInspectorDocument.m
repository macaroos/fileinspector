//
//  FileInspectorDocument.m
//  FileInspector
//
//  Created by Steve Hartwell on 8/28/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

#import "FileInspectorDocument.h"
#import "FileModel.h"
#import "FileInspectorWindowController.h"


@interface SHHFileEnumerationOperation: NSOperation
// NSOperation instances are more flexible
// than using -[NSOperationQueue operationWithBlock:]
// They are cancellable, and have a completion handler.
- (instancetype)initWithDirectory: (NSURL*)url
                      enumOptions: (NSDirectoryEnumerationOptions)options
                 insertionHandler: (void (^)(NSURL* fileURL))insertionHandler;
@end


@interface NSURL (SHHExtensions)
// work around a quirk in the way symlinks are resolved
- (NSURL*)URLByResolvingSymlinksInPathEvenInSlashPrivate;
@end


#pragma mark -

@interface SHHFileInspectorDocument ()
{
    NSManagedObjectModel* _managedObjectModel;
    NSManagedObjectContext* _managedObjectContext;
}
@property (nonatomic, assign) NSUInteger options;
@property (strong) NSOperationQueue* enumerationQueue;

@end

@implementation SHHFileInspectorDocument

- (instancetype)init
// override
// Called both when constructing a new Untitled document,
// and when opening an existing document.
{
    self = [super init];
    if (self) {
        self.options =
            NSDirectoryEnumerationSkipsHiddenFiles
            | NSDirectoryEnumerationSkipsSubdirectoryDescendants
            | NSDirectoryEnumerationSkipsPackageDescendants;
        self.enumerationQueue = [NSOperationQueue new];
        self.enumerationQueue.maxConcurrentOperationCount = 1;
    }
    return self;
}

- (instancetype)initWithContentsOfURL: (NSURL*)url
                               ofType: (NSString*)typeName
                                error: (NSError**)outError
// override
// Called by NSDocumentController
{
    self = [self init];
    if (self) {
        self.fileURL = url;
    }
    return self;
}

#if DEBUG
- (void)dealloc
{
    NSLog(@"%s", __func__);
}
#endif

#pragma mark -
#pragma mark Enumeration options

- (void)setIncludeHiddenItems: (BOOL)includeHiddenItems
{
    [self setOption:NSDirectoryEnumerationSkipsHiddenFiles
             enable:( ! includeHiddenItems)];
}
- (void)setRecurseSubdirectories: (BOOL)recurseSubdirectories
{
    [self setOption:NSDirectoryEnumerationSkipsSubdirectoryDescendants
             enable:( ! recurseSubdirectories)];
}
- (void)setRecursePackages: (BOOL)recursePackages
{
    [self setOption:NSDirectoryEnumerationSkipsPackageDescendants
             enable:( ! recursePackages)];
}
- (void)setOption: (NSDirectoryEnumerationOptions)option
           enable: (BOOL)enable
// bottleneck method to handle bit twiddling and kick off reloadData as needed.
{
    NSUInteger currentOptions = self.options;

    if (enable) {
        currentOptions |= option;
    }
    else {
        currentOptions &= ~(option);
    }

    if (self.options != currentOptions) {
        self.options = currentOptions;

        if (self.fileURL != nil) {
            [self reloadData];
        }
    }
}

- (void)reloadData
{
    // Step 1. Start an operation on our queue
    // to remove all the FileModel instances from the dataset.
    //
    [self.enumerationQueue addOperationWithBlock:
        ^{
            [self.managedObjectContext performBlockAndWait:
                ^{
                    // NSBatchDeleteRequest is 10.10+
                    NSError *error = nil;
                    NSFetchRequest *fetchAll = [NSFetchRequest new];
                    [fetchAll setEntity:
                        [SHHFileModel entityInContext:self.managedObjectContext]];
                    [fetchAll
                        setIncludesPropertyValues:NO]; // only managedObjectID
                    NSArray *allFiles =
                        [self.managedObjectContext executeFetchRequest:fetchAll
                                                                 error:&error];
                    for (NSManagedObject *aFile in allFiles) {
                        [self.managedObjectContext deleteObject:aFile];
                    }
                    [self.managedObjectContext save:&error];
                }];
        }];

    // Step 2. Begin the enumeration with the currently set enumeration options.

    // NSDirectoryEnumerator won't accept a symlink, and
    // -[NSURL URLByResolvingSymlinksInPath] won't resolve symlinks in /private,
    // so we have to do it the hard way.
    NSURL* dirURL = [self.fileURL URLByResolvingSymlinksInPathEvenInSlashPrivate];

    SHHFileEnumerationOperation* enumOperation =
        [[SHHFileEnumerationOperation alloc]
            initWithDirectory:dirURL
            enumOptions:self.options
            insertionHandler:
                ^(NSURL *fileURL) {
                    [self.managedObjectContext performBlockAndWait:
                        ^{
                            [SHHFileModel
                                modelInManagedObjectContext:
                                    self.managedObjectContext
                                withURL:
                                    fileURL];
                        }];
                }];
    // After enumeration completes, "save" the store to the in-memory dbase.
    enumOperation.completionBlock =
    ^{
        [self.managedObjectContext performBlockAndWait:
         ^{
             NSError* error;
             [self.managedObjectContext save:&error];
             [self updateChangeCount:NSChangeCleared];
         }];
    };

    // Kick off the enumeration once the remove block completes

    [self.enumerationQueue addOperation:enumOperation];
}

#pragma mark -
#pragma mark NSPersistentDocument

- (id)managedObjectModel
// override
// Not strictly necessary, but the default implementation gathers all of the
// data models it finds in the app bundle.  Convenient, but sloppy.
// This returns just the one model this code knows about.
{
    if (_managedObjectModel == nil) {

        NSString* modelPath =
            [[NSBundle bundleForClass:[self class]]
                pathForResource:@"FileInspectionModel" ofType:@"momd"];
        NSAssert(modelPath != nil, @"FileInspectionModel.momd not found");

        _managedObjectModel =
            [[NSManagedObjectModel alloc]
                initWithContentsOfURL:[NSURL fileURLWithPath:modelPath]];
        NSAssert(_managedObjectModel != nil, @"NSManagedObjectModel");
    }

    return _managedObjectModel;
}

- (NSManagedObjectContext*)managedObjectContext
// override
// Drat! All this code just so I can create the context
// with the NSMainQueueConcurrencyType, which it has to be
// because it updates NSArrayController and NSTableView instances.
// Or did I miss a memo?
{
    if (_managedObjectContext == nil) {

        _managedObjectContext =
            [[NSManagedObjectContext alloc]
                initWithConcurrencyType:NSMainQueueConcurrencyType];

        NSError* error = nil;
        NSPersistentStoreCoordinator *store =
            [[NSPersistentStoreCoordinator alloc]
                initWithManagedObjectModel:self.managedObjectModel];
        [store
            addPersistentStoreWithType:
                [self persistentStoreTypeForFileType:self.fileType]
            configuration:nil
            URL:nil
            options:nil
            error:&error];

        if (error) {
            // TODO: deal with nonnil error
        }
        
        _managedObjectContext.persistentStoreCoordinator = store;
        
        // no undo overhead needed, suppress.
        _managedObjectContext.undoManager = nil;
        
        [super setManagedObjectContext:_managedObjectContext];

#if DEBUG && 0
        [[NSNotificationCenter defaultCenter]
            addObserver:self
            selector:@selector(managedContextNotification:)
            name:nil
            object:_managedObjectContext];
#endif
    }
    
    return _managedObjectContext;
}

#if DEBUG
- (void)setManagedObjectContext:(NSManagedObjectContext*)managedObjectContext
// override
{
    if (managedObjectContext == nil) {
        [super setManagedObjectContext:managedObjectContext];
    }
    else {
        NSLog(@"%s: IGNORING %@", __func__, managedObjectContext);
    }
}

- (void)managedContextNotification: (NSNotification*)note
{
    NSLog(@"%s: %@", __func__, note.name);
}
#endif // DEBUG

- (NSString*)persistentStoreTypeForFileType: (NSString*)fileType
// override
// No need to hit the disk for our dataset, this will do nicely.
{
    return NSInMemoryStoreType;
}

#pragma mark -
#pragma mark NSDocument

- (void)makeWindowControllers
// override
// The UI aspect of an NSDocument instance: make some windows for your data.
{
    [self addWindowController:[SHHFileInspectorWindowController new]];
}

- (void)setFileURL: (NSURL*)dirURL
// override
{
    [super setFileURL:dirURL];
    
    if (dirURL == nil) {
        return;
    }

    [self reloadData];
}

- (NSUndoManager*)undoManager
// override
// No need for undo in our data model
{
    return nil;
}

@end

// -------------------------------------------------------------------------------


#pragma mark -

@implementation NSURL (SHHExtensions)

- (NSURL*)URLByResolvingSymlinksInPathEvenInSlashPrivate
// -[NSURL URLByResolvingSymlinksInPath] won't resolve symlinks in /private,
// so we have to do it the hard way.
{
    NSURL* resolvedURL = [self URLByResolvingSymlinksInPath];

    NSString* path = [resolvedURL path];
    NSError* err = nil;
    NSDictionary* pathAttributes =
        [[NSFileManager defaultManager] attributesOfItemAtPath:path error:&err];

    if ( ! [pathAttributes[NSFileType] isEqualToString:NSFileTypeSymbolicLink]) {
        return resolvedURL;
    }

    return [[self class] fileURLWithPath:
                [[path stringByDeletingLastPathComponent]
                    stringByAppendingPathComponent:
                        [[NSFileManager defaultManager]
                            destinationOfSymbolicLinkAtPath:path
                            error:&err]]];
}

@end

#pragma mark -

// I have a wicked fast implementation that is 4 times faster than
// this sequential enumeration.  It basically enumerates each subdirectory
// in its own NSOperation on the same queue.
// Unfortunately, even though it runs on a background-priority queue,
// it stalls the UI something fierce, and I haven't found a way to fix that.

@interface SHHFileEnumerationOperation ()

@property (strong) NSURL *dirURL;
@property (assign) NSDirectoryEnumerationOptions options;
@property (strong) void (^insertionHandler)(NSURL* fileURL);

@end

@implementation SHHFileEnumerationOperation

- (instancetype)initWithDirectory: (NSURL*)url
                      enumOptions: (NSDirectoryEnumerationOptions)options
                 insertionHandler: (void (^)(NSURL* fileURL))insertionHandler
{
    self = [super init];
    if (self) {
        self.dirURL = url;
        self.options = options;
        self.insertionHandler = insertionHandler;
    }
    return self;
}

- (void)main
// In addition to enumerating the file URLs themselves,
// we ask NSDirectoryEnumerator to pick up the file metadata
// such as file size, modification date, etc, and cache it
// in the NSURL instance.
// The SHHFileModel has a class property of the mapping between its
// data members and the keys that NSURL uses to store this metadata.
// So while constructing the enumerator, we pass the array of those
// NSURL metadata keys for it to collect for us.
{
    NSDirectoryEnumerator *dirEnum =
        [[NSFileManager defaultManager]
            enumeratorAtURL:self.dirURL
            includingPropertiesForKeys:[SHHFileModel.attributeMap allValues]
            options:self.options
            errorHandler:
                ^(NSURL *url, NSError *error)
                {
                    return YES;
                }];

    for (NSURL *aURL in dirEnum) {

        if ([self isCancelled]) {
            break;
        }

        NSNumber *isDirectory;
        [aURL getResourceValue:&isDirectory
                        forKey:NSURLIsDirectoryKey
                         error:NULL];

        if ( ! [isDirectory boolValue]) {
            self.insertionHandler(aURL);
        }
    }
}

@end

