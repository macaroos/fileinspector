//
//  FileInspectorWindowController.m
//  FileInspector
//
//  Created by Steve Hartwell on 8/28/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

#import "FileInspectorWindowController.h"

#import "FileInspectorDocument.h"

#import "DirectoryChooserViewController.h"
#import "FileInspectionListViewController.h"
#import "FileInfoViewController.h"

@interface SHHFileInspectorWindowController ()
                <NSToolbarDelegate, NSWindowDelegate, NSSplitViewDelegate>

@property (weak) IBOutlet NSSearchField* toolbarSearchField;
@property (weak) IBOutlet NSMenuItem* currentSearchMenuItem;

@property (weak) IBOutlet NSSplitView* contentSplitter;
@property (weak) IBOutlet NSPathControl* pathControl;

@property (weak)
    IBOutlet SHHDirectoryChooserViewController* dirChooserViewController;
@property (weak)
    IBOutlet SHHFileInspectionListViewController* listViewController;
@property (weak)
    IBOutlet SHHFileInfoViewController* fileInfoViewController;

@end

@implementation SHHFileInspectorWindowController

- (NSString*)windowNibName
{
    return @"FileInspectorWindow";
}

- (void)showWindow: (id)sender
// override
// Initially the window is small, and the Directory Chooser sheet covers it.
// This just poses the sheet and when it's dismissed, checks the return code.
// If it's Cancel, the window is closed; otherwise, the directoryURL is
// set on the document, and the window's subviews are set up.
{
    [super showWindow:sender];

    if ([self.document fileURL] != nil) {
        [self showInspection];
        return;
    }

    NSParameterAssert(self.window);
    NSParameterAssert(self.window.toolbar);
    //
    [self.window.toolbar setVisible:NO];

    NSParameterAssert(self.dirChooserViewController);
    NSParameterAssert(self.dirChooserViewController.view);
    NSParameterAssert(self.dirChooserViewController.view.window);
    //
    NSWindow* sheet = self.dirChooserViewController.view.window;

    [self.window
        beginSheet:sheet
        completionHandler:
            ^(NSModalResponse returnCode) {
                [sheet orderOut:self]; // block retains sheet
                if (returnCode != NSModalResponseContinue) {
                    [self close];
                    return;
                }
                [self.document setFileURL:
                    self.dirChooserViewController.directoryURL];
                [self showInspection];
            }];
}

- (void)showInspection
// Called once document.fileURL (which must really be a directory) is non-nil.
// Set up the main view (file list and file info pane) and the status bar.
{
    NSParameterAssert(self.document);
    NSParameterAssert([self.document fileURL]);

    NSParameterAssert(self.pathControl);
    //
    self.pathControl.URL = [self.document fileURL];

    NSParameterAssert(self.listViewController);
    //
    self.listViewController.representedObject = self.document;

    NSParameterAssert(self.contentSplitter);
    NSParameterAssert(self.listViewController.view);
    NSParameterAssert(self.fileInfoViewController);
    NSParameterAssert(self.fileInfoViewController.view);
    //
    [self.contentSplitter addSubview:self.listViewController.view];
    [self.contentSplitter addSubview:self.fileInfoViewController.view];

    [self.window.toolbar setVisible:YES];   // TODO: restore previous state
}

#pragma mark -
#pragma mark Menu and Toolbar actions

- (IBAction)toggleFileInfoView: (id)sender
{
    BOOL isHidden = self.fileInfoViewController.view.isHidden;

    self.fileInfoViewController.view.hidden = ( ! isHidden);
}

- (IBAction)showHiddenItemsAction: (NSButton*)sender
{
    [self.document setIncludeHiddenItems:([sender state] == NSOnState)];
}
- (IBAction)recurseSubdirectoriesAction: (NSButton*)sender
{
    [self.document setRecurseSubdirectories:([sender state] == NSOnState)];
}
- (IBAction)recursePackagesAction: (NSButton*)sender
{
    [self.document setRecursePackages:([sender state] == NSOnState)];
}

- (IBAction)searchfieldMenuItemAction: (NSMenuItem*)sender
{
    NSParameterAssert(self.currentSearchMenuItem);

    if (sender == self.currentSearchMenuItem) {
        return;
    }

    [self.toolbarSearchField.cell setPlaceholderString:sender.title];
    self.currentSearchMenuItem.state = NSOffState;
    self.currentSearchMenuItem = sender;
}

#pragma mark -
#pragma mark NSSplitViewDelegate

- (BOOL)splitView: (NSSplitView*)splitView canCollapseSubview: (NSView*)subview
{
    return subview == self.fileInfoViewController.view;
}

- (BOOL)splitView: (NSSplitView*)splitView
        shouldCollapseSubview: (NSView*)subview
        forDoubleClickOnDividerAtIndex: (NSInteger)dividerIndex
{
    return [self splitView:splitView canCollapseSubview:subview];
}

@end
