//
//  SHHFileInspectorViewController.m
//  FileInspector
//
//  Created by Steve Hartwell on 8/28/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

#import "FileInspectionListViewController.h"

#import "FileInspectorDocument.h"
#import "FileModel.h"


@interface SHHFileInspectionListViewController () <NSTableViewDelegate>

@property (weak) IBOutlet NSArrayController* fileListArrayController;
@property (weak) IBOutlet NSTableView* fileListTableView;

@end

@implementation SHHFileInspectionListViewController

- (SHHFileInspectorDocument*)model
{
    return self.representedObject;
}

#pragma mark -
#pragma mark Table Header Context menu item action

- (IBAction)tableHeaderContextMenuAction: (NSMenuItem*)sender
// all of the menu items call this selector
{
    // NSMenuItem doesn't implement <NSUserInterfaceItemIdentification>
    // but an identifier can be added as a User-Defined Runtime Attribute
    // in the xib.
    NSString* column_id = [sender valueForKey:@"identifier"];
    if (column_id) {
        NSTableColumn* column =
            [self.fileListTableView tableColumnWithIdentifier:column_id];
        if (column) {
            sender.state = (sender.state == NSOnState) ? NSOffState : NSOnState;
            column.hidden = (sender.state == NSOffState);
        }
    }
}

#pragma mark -
#pragma mark Table Row Context menu item actions

- (IBAction)revealInFinderAction: (NSMenuItem*)sender
{
    [self afterConfirmingSelectionSizePerformBlock:
        ^{
            [[NSWorkspace sharedWorkspace]
                activateFileViewerSelectingURLs:
                    [self arrayByEnumeratingSelectionKeyPath:@"url"]];
        }];
}

- (IBAction)openSelectedFilesAction: (id)sender
{
    [self afterConfirmingSelectionSizePerformBlock:
        ^{
            [[NSWorkspace sharedWorkspace]
                openURLs:[self arrayByEnumeratingSelectionKeyPath:@"url"]
                withAppBundleIdentifier:nil // @"com.apple.Finder"
                options:NSWorkspaceLaunchWithErrorPresentation
                additionalEventParamDescriptor:nil
                launchIdentifiers:NULL];
        }];
}

- (void)afterConfirmingSelectionSizePerformBlock: (void (^)())blockToPerform
{
    static NSString* const kNoWarnLgSelectKey = @"NoWarnLargeSelectionAction";

    if ([[NSUserDefaults standardUserDefaults] boolForKey:kNoWarnLgSelectKey]) {
        blockToPerform();
        return;
    }

    unsigned long selectionCount =
        self.fileListArrayController.selectionIndexes.count;
    if (selectionCount < 10) {
        blockToPerform();
        return;
    }

    NSAlert* largeSelectionAlert = [NSAlert new];
    largeSelectionAlert.messageText =
        [NSString stringWithFormat:
            NSLocalizedString(
                @"Are you sure you want to do this with %lu selected items?",
                @"Large selection warning"), selectionCount];
//    largeSelectionAlert.informativeText =
//        NSLocalizedString(@"...", @"");

    [largeSelectionAlert addButtonWithTitle:
        NSLocalizedString(@"Proceed", @"Large selection button title")];
    [largeSelectionAlert addButtonWithTitle:
        NSLocalizedString(@"Cancel",  @"Large selection button title")];

    largeSelectionAlert.showsSuppressionButton = YES;

    [largeSelectionAlert
        beginSheetModalForWindow:self.view.window
        completionHandler:
            ^(NSModalResponse returnCode) {

                NSInteger suppressState =
                    [[largeSelectionAlert suppressionButton] state];
                [[NSUserDefaults standardUserDefaults]
                    setBool:(suppressState == NSOnState)
                     forKey:kNoWarnLgSelectKey];
                
                if (returnCode == NSAlertFirstButtonReturn) {
                     blockToPerform();
                 }
            }];
}

#pragma mark -
#pragma mark Clipboard Actions

- (IBAction)copy: (id)sender
{
    [self setClipboardToArray:
         [self arrayByEnumeratingSelectionKeyPath:@"url"]];
}
- (IBAction)copyPathsToClipboardAction: (NSMenuItem*)sender
{
    [self setClipboardToArray:
         [self arrayByEnumeratingSelectionKeyPath:@"url.path"]];
}

- (void)setClipboardToArray: (NSArray*)objectArray
{
    NSPasteboard* pasteboard = [NSPasteboard generalPasteboard];
    [pasteboard clearContents];
    [pasteboard writeObjects:objectArray];
}

- (NSArray*)arrayByEnumeratingSelectionKeyPath: (NSString*)keyPath
{
    NSArray* arrangedObjects = self.fileListArrayController.arrangedObjects;
    NSIndexSet* selectionIndexes =
        self.fileListArrayController.selectionIndexes;
    NSMutableArray* itemArray =
        [NSMutableArray arrayWithCapacity:selectionIndexes.count];
    [selectionIndexes enumerateIndexesUsingBlock:
        ^(NSUInteger idx, BOOL *stop) {
            id item = [arrangedObjects[idx] valueForKeyPath:keyPath];
            if (item) {
                [itemArray addObject:item];
            }
        }];

    return itemArray;
}

@end

#pragma mark -

@interface SHHFileSizeTransformer: NSValueTransformer @end

@implementation SHHFileSizeTransformer

+ (Class)transformedValueClass
{
    return [NSString class];
}

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

- (id)transformedValue: (id)value
{
    long long sizeInBytes; // what NSByteCountFormatter takes

    if ([value respondsToSelector:@selector(longLongValue)]) {
        sizeInBytes = [value longLongValue];
    }
    else if ([value respondsToSelector:@selector(intValue)]) {
        sizeInBytes = [value intValue];
    }
    else if ([value respondsToSelector:@selector(stringValue)]) {
        return [value stringValue];
    }
    else {
        return [value description];
    }

    return [NSByteCountFormatter
                stringFromByteCount:sizeInBytes
                         countStyle:NSByteCountFormatterCountStyleFile];
}

@end
