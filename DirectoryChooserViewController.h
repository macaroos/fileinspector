//
//  DirectoryChooserViewController.h
//  FileInspector
//
//  Created by Steve Hartwell on 8/30/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SHHDirectoryChooserViewController: NSViewController

@property (nonatomic, readonly) NSURL* directoryURL;

@end
