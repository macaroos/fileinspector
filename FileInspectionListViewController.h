//
//  FileInspectorViewController.h
//  FileInspector
//
//  Created by Steve Hartwell on 8/28/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class SHHFileInspectorDocument;


@interface SHHFileInspectionListViewController: NSViewController

@property (strong, readonly) SHHFileInspectorDocument* model;

@end
