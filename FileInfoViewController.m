//
//  FileInfoViewController.m
//  FileInspector
//
//  Created by Steve Hartwell on 9/8/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

#import "FileInfoViewController.h"

#import <Quartz/Quartz.h>

#import "FileInspectionListViewController.h"


@interface SHHFileInfoViewController ()

@property (weak)
    IBOutlet SHHFileInspectionListViewController* listViewController;

@property (weak) IBOutlet QLPreviewView* previewView;

@end

@implementation SHHFileInfoViewController

- (void)loadView
{
    [super loadView];

    NSParameterAssert(self.previewView);

    // Interface Builder doesn't have a plugin for a QLPreviewView
    // despite having plugins for other Quartz/ImageKit views.
    // So no way to do the binding in the xib.
    // Have to do it by hand. Ewww.

    [self.previewView bind:@"previewItem"
                  toObject:self.listViewController
               withKeyPath:@"fileListArrayController.selection.url"
                   options:nil]; // no placeholder, etc
}

@end
