//
//  FileInspectorDocument.h
//  FileInspector
//
//  Created by Steve Hartwell on 8/28/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

// I have to keep reminding myself that NSDocument subclasses
// are not data models; they are really UI components which manage the
// persistence of data into and out of data models.
//

#import <Cocoa/Cocoa.h>

@interface SHHFileInspectorDocument: NSPersistentDocument

@property (nonatomic, assign) BOOL includeHiddenItems;
@property (nonatomic, assign) BOOL recurseSubdirectories;
@property (nonatomic, assign) BOOL recursePackages;

@end
